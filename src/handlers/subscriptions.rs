use sqlx::MySqlPool;
use async_trait::async_trait;
use youtube_dl::{YoutubeDl, YoutubeDlOutput};
use crate::utils::DbObject;
use crate::models::{Subscription, NewSubscription};

#[async_trait]
impl DbObject for Subscription {
	type NewDbObject = NewSubscription;

	async fn get(db: &MySqlPool) -> Result<Vec<Subscription>, sqlx::Error> {
		Ok(sqlx::query_as!(Subscription, "select * from subscription").fetch_all(db).await?)
	}

	async fn insert(db: &MySqlPool, input: NewSubscription) -> Result<Self, sqlx::Error> {
		let res = YoutubeDl::new(input.url)
			.youtube_dl_path("yt-dlp")
			.socket_timeout("15")
			.playlist_items(0)
			.extra_arg("-4")
			.run().unwrap();

		if let YoutubeDlOutput::Playlist(playlist) = res {
			if let Some(ref thumbs) = playlist.thumbnails {
				//if sqlx::query!("select * from subscription where channel_id = ?" playlist.id)
				let id = sqlx::query!("insert into subscription (
																 name,
																 channel_id,
																 url,
																 avatar)
															 values (?,?,?,?)",
					Some(playlist.uploader),
					Some(playlist.id),
					Some(playlist.webpage_url),
					Some(&thumbs.into_iter().find(
							|x| x.id == Some("avatar_uncropped".to_string()))
						.unwrap().url))
					.execute(db).await.unwrap().last_insert_id();
				Ok(sqlx::query_as!(Self, "select * from subscription where subscription_id = ?", id)
					.fetch_one(db).await.unwrap())
			} else { panic!() }
		} else { panic!() }
	}

	async fn delete(db: &MySqlPool, id: u64) -> Result<(), sqlx::Error> {
		sqlx::query!("delete from subscription where subscription_id = ?", id).execute(db).await?;
		Ok(())
	}
}
