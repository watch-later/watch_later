use chrono::{DateTime, Local, Utc, TimeZone, Timelike};
use serde::Deserialize;
use sqlx::MySqlPool;
use async_trait::async_trait;
use youtube_dl::{YoutubeDl, YoutubeDlOutput};
use crate::utils::{DbObject, shorten_string};
use crate::models::{Video, NewVideo, PartialVideo, Thumbnails};

#[derive(Deserialize)]
pub struct Params {
	pub external_id: Option<String>
}

struct Temp {
	video_id: u64,
	external_id: String,
	url: String,
	title: String,
	channel: String,
	channel_id: Option<String>,
	upload_date: String,
	release_timestamp: Option<f64>,
	t_max: String,
	t_hq: Option<String>,
	t_mq: Option<String>,
	t_sd: Option<String>,
	duration: u32,
	duration_string: String,
	description: String,
	progress: Option<f64>
}

impl Video {
	fn new(t: Temp) -> Self {
		if let Some(timestamp) = t.release_timestamp {
			let since = Local::now()
				.signed_duration_since(DateTime::<Utc>::from_timestamp(timestamp.round() as i64, 0).unwrap());

			Video {
				video_id: t.video_id,
				external_id: t.external_id,
				url: t.url,
				title: t.title,
				channel: t.channel,
				channel_id: t.channel_id,
				upload_date: t.upload_date,
				thumbnails: Thumbnails {
					maxres: t.t_max,
					hq: t.t_hq,
					mq: t.t_mq,
					sd: t.t_sd
				},
				secs_since: Some(since.num_seconds()),
				days_since: since.num_days(),
				duration: t.duration,
				duration_string: t.duration_string,
				description: t.description,
				release_timestamp: t.release_timestamp,
				progress: t.progress
			}
		} else {
			let utc = chrono::NaiveDate::parse_from_str(&t.upload_date, "%Y%m%d").unwrap().and_hms_opt(0,0,0).unwrap();
			let local_release = Local.from_utc_datetime(&utc);
			let since = Local::now().with_hour(0).unwrap().with_minute(0).unwrap() - local_release;

			Video {
				video_id: t.video_id,
				external_id: t.external_id,
				url: t.url,
				title: t.title,
				channel: t.channel,
				channel_id: t.channel_id,
				upload_date: t.upload_date,
				thumbnails: Thumbnails {
					maxres: t.t_max,
					hq: t.t_hq,
					mq: t.t_mq,
					sd: t.t_sd
				},
				secs_since: None,
				days_since: since.num_days(),
				duration: t.duration,
				duration_string: t.duration_string,
				description: t.description,
				release_timestamp: t.release_timestamp,
				progress: t.progress
			}
		}
	}
}

#[async_trait]
impl DbObject for Video {
	type NewDbObject = NewVideo;
	type PartialDbObject = PartialVideo;
	type ReqParams = Params;

	async fn get(db: &MySqlPool, params: Params) -> Result<Vec<Video>, sqlx::Error> {
		let videos = match params {
			Params { external_id: Some(external_id) } => {
				sqlx::query_as!(Temp, "select * from video where external_id = ?", external_id).fetch_all(db).await?
			},
			_ => {
				sqlx::query_as!(Temp, "select * from video").fetch_all(db).await?
			}
		};
		let mut res: Vec<Video> = Vec::new();

		for v in videos {
			res.push(Self::new(v))
		}
		Ok(res)
	}

	async fn insert(db: &MySqlPool, client: reqwest::Client, input: NewVideo) -> Result<Self, sqlx::Error> {
		let tmp = YoutubeDl::new(input.url.split('&').next().unwrap())
			.youtube_dl_path("yt-dlp")
			.socket_timeout("15")
			.extra_arg("-4")
			.extra_arg("--ignore-no-formats-error")
			.run();
		let res = YoutubeDl::new(input.url.split('&').next().unwrap())
			.youtube_dl_path("yt-dlp")
			.socket_timeout("15")
			.extra_arg("-4")
			.extra_arg("--ignore-no-formats-error")
			.run()
			.unwrap();

		if let YoutubeDlOutput::SingleVideo(video) = res {
			//Check if video is already submitted
			let submitted = sqlx::query!("select * from video where external_id = ?", video.id)
				.fetch_all(db).await.unwrap().len();
			if submitted == 1 {
				panic!("Video already submitted")
			}; //Actually fix this though

			//Thumbnails
			//This is a hacky solution because of difference in URL structure between twitter and youtube
			// Change this to use the thumbnail key. If it's not 16:9 walk the array backwards untily find a thumbnail that is
			let mut thumb_urls: Thumbnails = Default::default();
			if let Some(ref thumbs) = video.thumbnails {
				thumb_urls.maxres = thumbs[0].url.as_deref().unwrap().to_string();
				if thumbs.len() == 1 {
					thumb_urls.maxres = thumbs[0].url.as_deref().unwrap().to_string();
				} else {
					for thumb in thumbs {
						let url = thumb.url.as_deref().unwrap();
						match url.split("/").last().unwrap() {
							"maxresdefault.jpg" => {
								if client.get(url).send().await.unwrap().status() != 404 {
									thumb_urls.maxres = thumb.url.as_deref().unwrap().to_string()
								}
							},
							"mqdefault.jpg" => {
								thumb_urls.mq = Some(thumb.url.as_deref().unwrap().to_string())
							},
							_ => ()
						}
					}
				}
			} else { panic!("Video missing thumnails") }

			let description = if let Some(desc) = video.description {	desc } else { "".to_string() };
			let duration_string = if let Some(ds) = video.duration_string { ds } else {
				panic!("Video missing 'duration_string'")
			};
			let channel = if let Some(channel) = video.channel {
				channel
			} else {
				if let Some(uploader) = video.uploader {
					uploader
				} else {
					if let Some(extractor) = video.extractor_key {
						extractor
					} else {
						panic!("Missing channel name")
					}
				}
			};

			let id = sqlx::query!(
				"insert into video (
					 external_id,
					 url,
					 title,
					 channel,
					 channel_id,
					 upload_date,
					 release_timestamp,
					 t_max,
					 t_hq,
					 t_mq,
					 t_sd,
					 duration,
					 duration_string,
					 description)
				 values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
				video.id,
				video.webpage_url,
				shorten_string(&video.fulltitle),
				channel,
				Some(video.channel_id),
				video.upload_date,
				video.release_timestamp,
				thumb_urls.maxres,
				thumb_urls.hq,
				thumb_urls.mq,
				thumb_urls.sd,
				video.duration.unwrap().as_f64().unwrap().round(),
				duration_string.as_str(),
				description)
				.execute(db).await.unwrap().last_insert_id();

			Ok(Self::new(sqlx::query_as!(Temp, "select * from video where video_id = ?", id)
				.fetch_one(db).await.unwrap()))

		} else { panic!("youtube_dl failed") }
	}

	async fn update(db: &MySqlPool, id: u64, input: PartialVideo) -> Result<(), sqlx::Error> {
		sqlx::query!("update video set progress = ? where video_id = ?", input.progress, id).execute(db).await?;
		Ok(())
	}

	async fn delete(db: &MySqlPool, id: u64) -> Result<(), sqlx::Error> {
		sqlx::query!("delete from video where video_id = ?", id).execute(db).await?;
		Ok(())
	}
}
