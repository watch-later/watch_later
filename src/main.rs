use std::fs::File;
use std::time::Duration;
use actix_files::Files;
use actix_rt::spawn;
use actix_rt::time;
use actix_web::{web::{self, Data}, App, HttpServer};
use actix_cors::Cors;
use serde::{Deserialize};
use sqlx::mysql::MySqlPoolOptions;
use crate::models::*;
//use youtube_dl::{YoutubeDl, YoutubeDlOutput};

mod models;
mod utils;
mod handlers;

#[derive(Deserialize, Debug)]
struct Config {
	db_url: String,
	bind_address: Option<String>
}

#[actix_rt::main]
async fn main() -> Result<(), std::io::Error> {
	let cfg: Config = serde_json::from_reader(File::open("config.json").expect("Missing config.json")).expect("Syntax error in config.json");
	let pool = MySqlPoolOptions::new().max_connections(2).connect(&cfg.db_url).await.unwrap();
	let client = reqwest::Client::new();
	std::env::set_var("RUST_LOG", "debug");
	env_logger::init();

	//let output = YoutubeDl::new("https://www.youtube.com/user/ArloStuff")
	//	.youtube_dl_path("yt-dlp")
	//	.socket_timeout("15")
	//	.playlist_items(1)
	//	.run().unwrap();
	//if let YoutubeDlOutput::Playlist(playlist) = output {
	//	dbg!(playlist.id);
	//}

	spawn(async {
		let mut interval = time::interval(Duration::from_secs(1200));
		loop {
			interval.tick().await;
			println!("Timer started");
		}
	});

	HttpServer::new(move || {
		App::new()
			.app_data(Data::new(pool.clone()))
			.app_data(Data::new(client.clone()))
			.wrap(
				Cors::default()
				.allow_any_origin()
				.allow_any_method()
				.allow_any_header()
				.allowed_methods(vec!["POST"])
				.max_age(3600))
			.service(
				web::resource("videos")
					.route(web::get().to(utils::get::<Video>))
					.route(web::post().to(utils::add::<Video>))
			)
			.service(
				web::resource("videos/{id}")
					.route(web::put().to(utils::put::<Video>))
					.route(web::delete().to(utils::delete::<Video>))
			)
			//.service(
			//	web::resource("subscriptions")
			//		.route(web::get().to(utils::get::<Subscription>))
			//		.route(web::post().to(utils::add::<Subscription>))
			//)
			//.service(
			//	web::resource("subscriptions/{id}")
			//		.route(web::delete().to(utils::delete::<Subscription>))
			//)
			.service(Files::new("", "./static/").index_file("index.html"))
	})
	.bind(cfg.bind_address.unwrap_or("0.0.0.0:8090".to_string()))?
	.run()
	.await
}
