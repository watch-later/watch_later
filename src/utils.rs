use std::marker::Sized;
use actix_web::{web, error, HttpResponse, Error};
use sqlx::MySqlPool;
use serde::Serialize;
use async_trait::async_trait;

#[derive(Serialize, Debug)]
pub struct RowId { pub id: u64 }

pub async fn get<Model: DbObject>(
	db: web::Data<MySqlPool>,
	params: web::Query<Model::ReqParams>
) -> Result<HttpResponse, Error> {
	Ok(Model::get(&db, params.into_inner())
		.await
		.map(|db_object| HttpResponse::Ok().json(db_object))
		.map_err(|_| error::ErrorInternalServerError("An internal server error occurred")).unwrap())
}
pub async fn add<Model: DbObject>(
	db: web::Data<MySqlPool>,
	client: web::Data<reqwest::Client>,
	input: web::Json<Model::NewDbObject>
) -> Result<HttpResponse, Error> {
	let x: Model::NewDbObject = input.0;
	Ok(Model::insert(&db, client.get_ref().clone(), x)
		.await
		.map(|db_object| HttpResponse::Created().json(db_object))
		.map_err(|_| error::ErrorInternalServerError("An internal server error occurred")).unwrap())
}
pub async fn put<Model: DbObject>(db: web::Data<MySqlPool>, id: web::Path<u64>, input: web::Json<Model::PartialDbObject>)
-> Result<HttpResponse, Error> {
	Ok(Model::update(&db, id.into_inner(), input.into_inner())
		.await
		.map(|_| HttpResponse::Ok().finish())
		.map_err(|_| error::ErrorInternalServerError("An internal server error occurred")).unwrap())
}
pub async fn delete<Model: DbObject>(db: web::Data<MySqlPool>, id: web::Path<u64>) -> Result<HttpResponse, Error> {
	Ok(Model::delete(&db, id.into_inner())
		.await
		.map(|_| HttpResponse::Ok().finish())
		.map_err(|_| error::ErrorInternalServerError("An internal server error occurred")).unwrap())
}

pub fn shorten_string(s: &str) -> String {
	if s.len() > 100 {
		format!("{}...", &s[..97])
	} else {
		s.to_string()
	}
}

#[async_trait]
pub trait DbObject: Serialize {
	type NewDbObject;
	type PartialDbObject;
	type ReqParams;

	async fn get(db: &MySqlPool, params: Self::ReqParams) -> Result<Vec<Self>, sqlx::Error> where Self: Sized;
	async fn insert(db: &MySqlPool, client: reqwest::Client, inputs: Self::NewDbObject) -> Result<Self, sqlx::Error> where Self: Sized;
	async fn update(db: &MySqlPool, id: u64, input: Self::PartialDbObject) -> Result<(), sqlx::Error>;
	async fn delete(db: &MySqlPool, id: u64) -> Result<(), sqlx::Error>;
}
