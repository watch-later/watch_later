use serde::{Serialize, Deserialize};
use serde_with::{serde_as, NoneAsEmptyString};

#[serde_as]
#[derive(Serialize, Debug)]
pub struct Video {
	pub video_id: u64,
	pub external_id: String,
	pub url: String,
	pub title: String,
	pub channel: String,
	#[serde_as(as = "NoneAsEmptyString")]
	pub channel_id: Option<String>,
	pub upload_date: String,
	pub release_timestamp: Option<f64>,
	pub thumbnails: Thumbnails,
	pub secs_since: Option<i64>,
	pub days_since: i64,
	pub duration: u32,
	pub duration_string: String,
	pub description: String,
	pub progress: Option<f64>
}

#[derive(Deserialize, Debug)]
pub struct NewVideo {
	pub url: String
}

#[derive(Deserialize, Debug)]
pub struct PartialVideo {
	pub progress: f64
}

#[derive(Serialize, Debug)]
pub struct Subscription {
	pub subscription_id: u64,
	pub name: String,
	pub channel_id: String,
	pub url: String,
	pub avatar: String
}

#[derive(Deserialize, Debug)]
pub struct NewSubscription {
	pub url: String
}

#[serde_as]
#[derive(Serialize, Default, Debug)]
pub struct Thumbnails {
	pub maxres: String,
	#[serde_as(as = "NoneAsEmptyString")]
	pub hq: Option<String>,
	#[serde_as(as = "NoneAsEmptyString")]
	pub mq: Option<String>,
	#[serde_as(as = "NoneAsEmptyString")]
	pub sd: Option<String>,
}
