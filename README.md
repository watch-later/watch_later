# The self hosted watch later list
![Screenshot](screenshot.png)
![Screenshot](screenshot2.png)


Watch Later is a simple application that aims to provide meta data about videos you would like to watch later. It also comes with a basic web client but the intent is to use it with many "frontends". It _should_ support any website that yt-dlp supports. It's recommended that you also add the [Watch Later Chrome extension](https://git.najer.info/watch-later/watch_later_chrome) so you can easily save any youtube URL by right clicking it.

## Installation
### Clone the repo
``` git clone https://git.najer.info/watch-later/watch_later/ ```
### Build the binary
``` cargo build --release ```
### Write a systemd service for it
```
[Unit]
Description=Self hosted watch later list
After=syslog.target network.target

[Service]
Type=simple
User=twiclo
WorkingDirectory=/srv/watch_later/
ExecStart=/srv/watch_later/target/release/watch_later
Restart=on-abort

[Install]
WantedBy=multi-user.target
```
### Set up a reverse proxy for the service
The service exposes itself on port 8090
```
(Apache)
<VirtualHost *:80>
  ServerName wl.your.server
  ProxyPass / http://localhost:8090/ connectiontimeout=5 timeout=30
  ProxyPreserveHost on
</VirtualHost>

<VirtualHost *:443>
  ServerName wl.your.server
  ProxyPass / http://localhost:8090/ connectiontimeout=5 timeout=30

	#<Location />
	#	AuthType Basic
	#	AuthName "Watch Later"
	#	AuthUserFile "/etc/httpd/passwd/passwords
	#	Require user youruser
	#	Require ip 555.555.555.555
	#</Location>
</VirtualHost>
```
## Password protecting the website
I have run this for years with no password and haven't run into any issues. If you're concerned you can set up a password using your web server of choice. Commented out above are some examples of how that can be done under the `Location` target

## Coming soon

- Support for playlists, including a subscription feed
- Custom build of newpipe to add WL as a feed
