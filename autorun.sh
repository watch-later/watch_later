while true; do
	kill -9 %1;
	clear;
	cargo build --color always -q 2>&1 | colorfold -w $(tput cols) | head -n $(tput lines) | head -c -1;

	STATUS=${PIPESTATUS[0]};
	if [[ $STATUS -eq 0 ]]; then
		cargo run &
	fi
	inotifywait -q -e close_write src/* src/**/* migrations/* Cargo.toml;
done;
