ALTER TABLE video CHANGE date upload_date VARCHAR(45) NOT NULL;
ALTER TABLE video CHANGE timestamp release_timestamp BIGINT;
