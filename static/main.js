if ('serviceWorker' in navigator) {
	navigator.serviceWorker.register('sw.js')
		.then(function(registration) {
			console.log('Service Worker registered with scope:', registration.scope);
		})
		.catch(function(error) {
			console.log('Service Worker registration failed:', error);
		});
}

let section = document.getElementById('videoBlock')

var videos = fetch("videos").then(res => {
	res.json().then(videos => {
		for (video of videos) {
			addItem(video)
		}
		document.body.appendChild(section);
	})
})

//var subButton = document.getElementById("subs");
//subButton.addEventListener("click", () => {
//	while(section.hasChildNodes()) {
//		section.removeChild(section.lastChild);
//	}
//	fetch("subscriptions").then(res => {
//		res.json().then(subs => {
//			console.log(subs)
//			for (sub of subs) {
//				addSub(sub)
//			}
//		})
//	})
//})

function addSub(sub) {
	var subDiv = document.createElement("figure")
	subDiv.className = "subscription"

	var avatar = document.createElement("img")
	avatar.className = "avatar"
	avatar.src = sub.avatar
	var avatarLink = document.createElement("a")
	avatarLink.href = sub.url

	var caption = document.createElement("figcaption")
	var title = document.createElement("h3")
	var titleText = document.createTextNode(sub.name)
	var titleLink = document.createElement("a")
	titleLink.href = sub.url

	var trashButton = document.createElement("button")
	trashButton.addEventListener("click", remove.bind(null, sub.channel_id, subDiv))
	trashButton.className = "trashButton"
	trashButton.id = "trashButton"
	var trashButtonText = document.createTextNode("Remove")
	trashButton.appendChild(trashButtonText)

	title.appendChild(titleText);
	avatarLink.appendChild(avatar);
	subDiv.appendChild(avatarLink);
	titleLink.appendChild(title);
	caption.appendChild(titleLink);
	subDiv.appendChild(caption);
	subDiv.appendChild(trashButton);
	section.appendChild(subDiv);
}

function addItem(video) {
	var videoDiv = document.createElement("figure")
	videoDiv.className = "video"

	var thumbnail = document.createElement("img")
	thumbnail.className = "thumbnail"
	if(video.thumbnails.maxres) {
		thumbnail.src = video.thumbnails.maxres
	} else {
		thumbnail.src = video.thumbnails.mq
	}
	var thumbLink = document.createElement("a")
	thumbLink.href = video.url

	var caption = document.createElement("figcaption")
	var title = document.createElement("h3")
	var titleText = document.createTextNode(video.title)
	var uploadText = document.createTextNode(`${video.duration_string} • ${timeSince(video.days_since, video.secs_since)}`)
	uploadText.className = "uploadText"
	var titleLink = document.createElement("a")
	titleLink.href = video.url

	if(video.timestamp) {
		if (video.url.includes("odysee.com")) {
			titleLink.href = titleLink.href + "?t=" + video.timestamp
			thumbLink.href = thumbLink.href + "?t=" + video.timestamp
		} else {
			titleLink.href = titleLink.href + "&t=" + video.timestamp + "s"
			thumbLink.href = thumbLink.href + "&t=" + video.timestamp + "s"
		}
	}

	var channel = document.createElement("section")
	var channelText = document.createTextNode(video.channel)
	var channelLink = document.createElement("a")
	//Fix this
	channelLink.href = `https://youtube.com/channel/${video.channel_id}`

	var trashButton = document.createElement("button")
	trashButton.addEventListener("click", remove.bind(null, video.video_id, videoDiv))
	trashButton.className = "trashButton"
	trashButton.id = "trashButton"
	var trashButtonText = document.createTextNode("Remove")
	trashButton.appendChild(trashButtonText)

	title.appendChild(titleText);
	channel.appendChild(channelText);
	channelLink.appendChild(channel);
	thumbLink.appendChild(thumbnail);
	videoDiv.appendChild(thumbLink);
	titleLink.appendChild(title);
	caption.appendChild(titleLink);
	caption.appendChild(channelLink);
	caption.appendChild(uploadText);
	videoDiv.appendChild(caption);
	videoDiv.appendChild(trashButton);
	section.appendChild(videoDiv);
}

function remove(videoId, element) {
	fetch(`videos/${videoId}`, {
		method: "DELETE",
	}).then(res => {
		element.remove()
	})
}

document.getElementById('addVidButton').addEventListener("click", function(){
	let submitValue = document.getElementById('addVidBox').value;

	fetch("videos", {
		method: "POST",
		headers: { "Content-Type": "application/json" },
		body: JSON.stringify({ "url": submitValue })
	}).then(res => {
		res.text().then((text) => {
			addItem(JSON.parse(text));
		})
	})
})

window.addEventListener('load', () => {
	navigator.clipboard.readText()
	.then(text => {
		if (text.includes('youtube.com')) {
			document.getElementById('addVidBox').value = text;
		}
	})
})

document.addEventListener('visibilitychange', () => {
	if (document.visibilityState === 'visible') {
		setTimeout(() => {
			navigator.clipboard.readText()
			.then(text => {
				if (text.includes('youtube.com')) {
					document.getElementById('addVidBox').value = text;
				}
			})
		}, 1000)
	}
})

// document.getElementById('addVidBox').addEventListener

function timeSince(days, seconds) {
	if (!seconds) {
		seconds = days * 60 * 60 * 24
	}
	let suffix = seconds < 0 ? "from now" : "ago";
	seconds = Math.abs(seconds);

	var interval = seconds / (365 * 24 * 60 * 60);
	if (interval >= 1) {
		let floor = Math.floor(interval);
		return floor === 1 ? floor + " year " + suffix : floor + " years " + suffix;
	}

	interval = seconds / (30 * 24 * 60 * 60);
	if (interval >= 1) {
		let floor = Math.floor(interval);
		return floor === 1 ? floor + " month " + suffix : floor + " months " + suffix;
	}

	interval = seconds / (7 * 24 * 60 * 60);
	if (interval >= 1) {
		let floor = Math.floor(interval);
		return floor === 1 ? floor + " week " + suffix : floor + " weeks " + suffix;
	}

	interval = seconds / (24 * 60 * 60);
	if (interval >= 1 || seconds == 0) {
		let floor = Math.floor(interval);
		return floor === 1 ? floor + " day " + suffix : floor + " days " + suffix;
	}

	interval = seconds / (60 * 60);
	if (interval >= 1) {
		let floor = Math.floor(interval);
		return floor === 1 ? floor + " hour " + suffix : floor + " hours " + suffix;
	}

	interval = seconds / 60;
	if (interval >= 1) {
		let floor = Math.floor(interval);
		return floor === 1 ? floor + " minute " + suffix : floor + " minutes " + suffix;
	}

	return Math.floor(seconds) === 1 ? "1 second " + suffix : Math.floor(seconds) + " seconds " + suffix;
}

const btn = document.querySelector('.btn-toggle');
const currentTheme = localStorage.getItem("theme");

if (currentTheme == "osrs") {
	document.body.classList.add("osrs");
}

btn.addEventListener('click', function() {
  document.body.classList.toggle('osrs');
	document.getElementById("addVidBox").placeholder = "*"
  localStorage.setItem("theme", "osrs");
})
